package mapwiki.config;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@Deprecated
public final class Logging {
	public static final String LOGGER_NAME = "net.patrickpang.mapwiki";
	
	public static Logger getLogger() {
		Logger log = Logger.getLogger(LOGGER_NAME);
		log.setUseParentHandlers(false);
		log.setLevel(Level.ALL);
		
		ConsoleHandler ch = new ConsoleHandler();
		ch.setLevel(Level.INFO);
		log.addHandler(ch);
		
		try {
			FileHandler fh = new FileHandler("%t/mapwiki.log", false);
			fh.setFormatter(new SimpleFormatter());
			fh.setLevel(Level.ALL);
			log.addHandler(fh);
		} catch (IOException ex) {
			log.severe("IOException when creating log file.");
		}
		
		return log;
	}
}
