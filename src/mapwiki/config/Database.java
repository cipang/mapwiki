package mapwiki.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Deprecated
public final class Database {
	// For home use.
	public static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
	public static final String CONN_STRING = "jdbc:mysql://192.168.123.1/wikidb";
	public static final String USERNAME = "wikiuser";
	public static final String PASSWORD = "wiki1111";
	
	// For umsurvey server.
//	public static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
//	public static final String CONN_STRING = "jdbc:mysql://umsurvey.umac.mo/test";
//	public static final String USERNAME = "cipang";
//	public static final String PASSWORD = "wiki1111";
	
	private static String driverName = DRIVER_NAME;
	private static String connString = CONN_STRING;
	private static String username = USERNAME;
	private static String password = PASSWORD;
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driverName);
		Connection conn = DriverManager.getConnection(connString, username, password);
		return conn;
	}
	
	public static void init(String connection, String user, String pw, String driver) {
		driverName = driver;
		connString = connection;
		username = user;
		password = pw;
	}
	
	public static void init(String connection, String user, String pw) {
		init(connection, user, pw, DRIVER_NAME);
	}
}
