mysql -ch sql-s1-user enwiki_p
mysql -ch sql-s3-user dawiki_p
mysql -ch sql-s5-user dewiki_p
mysql -ch sql-s5-user svwiki_p
mysql -ch sql-s5-user zhwiki_p

mysqldump -u robertb -p -h sql-s5-user u_robertb tmp_page_author_count_sv tmp_cat_author_count_sv > sv.sql


/*
 timestamps:
   da: 20110112143000
   de: 20110111165216
   en: 20110115080216
   sv: 20110111133324
   zh: 20110110220649
 */

CREATE TABLE IF NOT EXISTS u_robertb.tmp_page_author_count_en (
 page_id int(11) NOT NULL,
 author_count int(11) NOT NULL,
 PRIMARY KEY  (page_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT /* SLOW_OK */ INTO u_robertb.tmp_page_author_count_en
SELECT r.rev_page, COUNT( DISTINCT r.rev_user_text ) AS c
FROM revision r, page p
WHERE r.rev_timestamp <= '20110115080216' AND
	r.rev_page = p.page_id AND p.page_namespace = 0
GROUP BY rev_page;

CREATE TABLE u_robertb.tmp_cat_author_count_en (
cat_page_id INT( 10 ) NOT NULL ,
avg_author DOUBLE NOT NULL ,
PRIMARY KEY ( cat_page_id )
) ENGINE = MYISAM ;

insert into u_robertb.tmp_cat_author_count_en
select p.page_id, avg(t.author_count)
from page p, categorylinks c, u_robertb.tmp_page_author_count_en t
where p.page_namespace = 14 and p.page_title = c.cl_to
  and c.cl_from = t.page_id
group by page_id;

select t.*, p.page_title from u_robertb.tmp_cat_author_count_en t
left join page p on p.page_id = t.cat_page_id;

-- Helping SQL statements.

SELECT p.page_id, page_namespace, page_title, t.author_count
FROM page p, u_robertb.tmp_page_author_count_en t 
WHERE p.page_id = t.page_id and p.page_namespace = 0;

SELECT avg(t.author_count) FROM u_robertb.tmp_page_author_count_en t
where t.page_id in (select c.cl_from from categorylinks c where c.cl_to = 'Science')
